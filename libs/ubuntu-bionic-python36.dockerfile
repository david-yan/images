FROM registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-bionic-python36

# Install TensorFlow GPU version, and appropriate versions of Keras, Theano, Pytorch.
# We also install other fixed versions of ML libraries. Should be kept in sync with
# "run_validation.py" script in primitives repository.
RUN pip3 install \
 tensorflow-gpu==2.0.0  \
 keras==2.3.1 \
 torch==1.3.1 \
 Theano==1.0.4 \
 scikit-learn==0.21.3 \
 numpy==1.17.3 \
 pandas==0.25.2 \
 networkx==2.4 \
 pyarrow==0.15.1 \
 scipy==1.3.1 \
 Pillow==6.2.1 \
 Cython==0.29.14 \
 && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

ARG org_datadrivendiscovery_public_source_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_SOURCE_COMMIT=$org_datadrivendiscovery_public_source_commit

ARG org_datadrivendiscovery_public_base_digest
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_BASE_DIGEST=$org_datadrivendiscovery_public_base_digest
