FROM registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-bionic-python36

ENV D3M_INTERFACE_VERSION=v2019.2.18

RUN \
 pip3 install --process-dependency-links git+https://gitlab.com/datadrivendiscovery/d3m.git@$D3M_INTERFACE_VERSION#egg=d3m && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm
