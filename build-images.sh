#!/bin/sh -e

docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"

# For each directory with Dockerfiles ...
for IMAGE_NAME in "$@"; do
  # ... for each Dockerfile in it, we build an image.
  for IMAGE_FILE in $IMAGE_NAME/*.dockerfile ; do
    IMAGE_BASENAME="$(basename $IMAGE_FILE)"
    IMAGE_LABEL=${IMAGE_BASENAME%.dockerfile}

    echo "Processing $IMAGE_NAME:$IMAGE_LABEL (filename $IMAGE_FILE)"

    CURRENT_IMAGE_BASE=$(grep FROM "$IMAGE_FILE" | awk '{print $2}')

    # We pull the latest base image we can find in the registry to get its digest.
    docker pull "$CURRENT_IMAGE_BASE"
    CURRENT_IMAGE_BASE_DIGEST=$(docker inspect --format='{{index .RepoDigests 0}}' "$CURRENT_IMAGE_BASE")

    # We pull the latest image for this Dockerfile we can find in the registry to get its commit SHA
    # and the digest of the base image used to build it.
    if docker pull "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_LABEL"; then
      # Commit stored in a label in the image. "org.datadrivendiscovery.source-commit" is for backwards
      # compatibility for before we had a public images repository. Private images are layered on
      # top of public ones, so we try in this order.
      IMAGE_COMMIT_SHA=$(docker inspect --format='{{or (index .Config.Labels "org.datadrivendiscovery.private.source-commit") (index .Config.Labels "org.datadrivendiscovery.public.source-commit") (index .Config.Labels "org.datadrivendiscovery.source-commit")}}' "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_LABEL")
      # Base image digest stored in a label in the image. "org.datadrivendiscovery.base-digest" is for
      # backwards compatibility for before we had a public images repository. Private images are layered
      # on top of public ones, so we try in this order.
      IMAGE_BASE_DIGEST=$(docker inspect --format='{{or (index .Config.Labels "org.datadrivendiscovery.private.base-digest") (index .Config.Labels "org.datadrivendiscovery.public.base-digest") (index .Config.Labels "org.datadrivendiscovery.base-digest")}}' "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_LABEL")
    else
      # The first commit in the repository.
      IMAGE_COMMIT_SHA=$(git rev-list --max-parents=0 HEAD)
      # No base image digest.
      IMAGE_BASE_DIGEST=""
    fi

    echo "Current image base digest: $CURRENT_IMAGE_BASE_DIGEST"
    echo "Image base digest: $IMAGE_BASE_DIGEST"

    echo "CI commit: $CI_COMMIT_SHA"
    echo "Image commit: $IMAGE_COMMIT_SHA"

    echo "Files changed:"
    git diff --name-only "$IMAGE_COMMIT_SHA" "$CI_COMMIT_SHA"

    # REBUILD_IMAGE environment variable is set by a pipeline trigger which is called from other repositories when
    # they want to rebuild a particular image.
    # Example:
    # curl -X POST -F token=$TRIGGER_TOKEN -F ref=master -F variables[REBUILD_IMAGE]=image_name:image_label https://gitlab.com/api/v4/projects/XXX/trigger/pipeline
    echo "Rebuild image environment variable: $REBUILD_IMAGE"

    # We rebuild if any of the following:
    #  * REBUILD_IMAGE variable was set to the name of the image
    #  * There is a new base image (digests do not match)
    #  * Dockerfile for the image changed since the last build
    #  * Any non-Dockerfile file in the image directory changed since the last build
    if [ "$REBUILD_IMAGE" = "$IMAGE_NAME:$IMAGE_LABEL" ] || \
        [ "$CURRENT_IMAGE_BASE_DIGEST" != "$IMAGE_BASE_DIGEST" ] || \
        git diff --name-only "$IMAGE_COMMIT_SHA" "$CI_COMMIT_SHA" | grep --fixed-strings --line-regexp --quiet "$IMAGE_FILE" || \
        git diff --name-only "$IMAGE_COMMIT_SHA" "$CI_COMMIT_SHA" | grep "^$IMAGE_NAME/" | grep --invert-match --quiet "^$IMAGE_NAME/.+\\.dockerfile$"; then
      SHOULD_REBUILD="true"
    else
      SHOULD_REBUILD="false"
    fi

    # If there is a custom build script, use it. Script can decide to build or skip building the image.
    if [ -x "$IMAGE_NAME/build.sh" ]; then
      "$IMAGE_NAME/build.sh" "$SHOULD_REBUILD" "$IMAGE_NAME" "$IMAGE_LABEL" "$IMAGE_FILE" "$CURRENT_IMAGE_BASE" "$CURRENT_IMAGE_BASE_DIGEST"
    elif [ "$SHOULD_REBUILD" = "true" ]; then
      echo "Building Docker image"

      docker build --tag="$CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_LABEL" \
       --label="org.datadrivendiscovery.public.source-commit=$CI_COMMIT_SHA" \
       --label="org.datadrivendiscovery.public.base-digest=$CURRENT_IMAGE_BASE_DIGEST" \
       --build-arg="org_datadrivendiscovery_public_source_commit=$CI_COMMIT_SHA" \
       --build-arg="org_datadrivendiscovery_public_base_digest=$CURRENT_IMAGE_BASE_DIGEST" \
       --file="$IMAGE_FILE" "$IMAGE_NAME"
      docker push "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_LABEL"
    else
      echo "Skipping"
    fi
  done
done
