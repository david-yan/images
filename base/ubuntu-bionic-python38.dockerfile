FROM ubuntu:bionic

ENV DEBIAN_FRONTEND=noninteractive

# apt-utils seems missing and warnings are shown, so we install it.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes apt-utils tzdata locales file sudo && \
 echo 'UTC' > /etc/timezone && \
 rm /etc/localtime && \
 dpkg-reconfigure tzdata && \
 apt-get upgrade --yes --force-yes && \
 rm -f /etc/cron.weekly/fstrim && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Prevent Python packages to be installed through APT.
COPY ./etc/apt/preferences.d/no-python2 /etc/apt/preferences.d/no-python2

# Prevent Python 3.6 packages to be installed through APT.
COPY ./etc/apt/preferences.d/no-python36 /etc/apt/preferences.d/no-python36

# General dependencies for building dependencies, locales, and some utilities.
RUN sed -i 's/^# deb-src/deb-src/' /etc/apt/sources.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends swig git build-essential cmake wget ssh tar gzip ca-certificates unzip curl libcurl4-openssl-dev libyaml-dev libssl-dev equivs vim-tiny && \
 locale-gen --no-purge en_US.UTF-8 && \
 update-locale LANG=en_US.UTF-8 && \
 echo locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8 | debconf-set-selections && \
 echo locales locales/default_environment_locale select en_US.UTF-8 | debconf-set-selections && \
 dpkg-reconfigure locales && \
 update-alternatives --install /usr/bin/vim vim /usr/bin/vim.tiny 10 && \
 curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash && \
 apt-get install --yes --force-yes git-lfs && \
 git lfs install && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Docker in Docker.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes apt-transport-https ca-certificates && \
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
 echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" > /etc/apt/sources.list.d/docker.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes docker-ce && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Set locale to UTF-8 which makes Python read text in UTF-8 and not ASCII, by default.
ENV LC_ALL=en_US.UTF-8

# Installing Python 3.8.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes python3.8 python3.8-dev && \
 update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.8 10 && \
 update-alternatives --install /usr/bin/python3-config python3-config /usr/bin/python3.8-config 10 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Dummy packages to satisfy unnecessary dependencies on Python 3.6.
COPY ./equivs/python3 /tmp/python3
COPY ./equivs/python3-dev /tmp/python3-dev
RUN cd /tmp && \
 equivs-build python3 && \
 dpkg -i python3-dummy_1.0_all.deb && \
 equivs-build python3-dev && \
 dpkg -i python3-dev-dummy_1.0_all.deb && \
 cd / && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes dh-python python3-distutils && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Installing pip 19.3.1.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes python3-pip python3-openssl && \
 pip3 install pip==19.3.1 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Base Python packages (those dependencies from d3m core package which are needed also in testing image).
# We install PyYAML from source so that a fast C implementation is compiled as well.
# We install a fork of docker-py (from version 2.7.0) which has support for enabling GPUs.
RUN pip3 install git+https://github.com/mitar/docker-py.git#egg=docker[tls] && \
 pip3 install --upgrade setuptools && \
 pip3 install frozendict==1.2 && \
 pip3 install --no-binary :all: PyYAML==5.1.2 && \
 pip3 install pycurl==7.43.0.3 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Dummy package to satisfy unnecessary dependency on Python 2.
# See: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=891712
COPY ./equivs/python-dev /tmp/python-dev
RUN cd /tmp && \
 equivs-build python-dev && \
 dpkg -i python-dev-dummy_1.0_all.deb && \
 cd / && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Install CUDA 10.0.
# Based on:
# https://gitlab.com/nvidia/container-images/cuda/blob/ubuntu18.04/10.0/base/Dockerfile
# https://gitlab.com/nvidia/container-images/cuda/blob/ubuntu18.04/10.0/devel/Dockerfile
# https://gitlab.com/nvidia/container-images/cuda/blob/ubuntu18.04/10.0/runtime/Dockerfile
# https://github.com/tensorflow/tensorflow/blob/master/tensorflow/tools/dockerfiles/dockerfiles/gpu.Dockerfile
ENV CUDA_VERSION=10.0.130
ENV CUDNN_VERSION=7.6.2.24-1
ENV NCCL_VERSION=2.4.2
ENV CUDA_PKG_VERSION=10-0=$CUDA_VERSION-1
RUN \
 curl -fsSL https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub | apt-key add - && \
 echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/cuda.list && \
 echo "deb https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/nvidia-ml.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends \
  cuda-cudart-$CUDA_PKG_VERSION \
  cuda-compat-10-0 \
  cuda-command-line-tools-$CUDA_PKG_VERSION \
  cuda-cublas-$CUDA_PKG_VERSION \
  cuda-cufft-$CUDA_PKG_VERSION \
  cuda-curand-$CUDA_PKG_VERSION \
  cuda-cusolver-$CUDA_PKG_VERSION \
  cuda-cusparse-$CUDA_PKG_VERSION \
  cuda-libraries-$CUDA_PKG_VERSION \
  cuda-nvtx-$CUDA_PKG_VERSION \
  libcudnn7=$CUDNN_VERSION+cuda10.0 \
  libfreetype6-dev \
  libhdf5-serial-dev \
  libzmq3-dev \
  pkg-config \
  software-properties-common \
  cuda-libraries-dev-$CUDA_PKG_VERSION \
  cuda-nvml-dev-$CUDA_PKG_VERSION \
  cuda-minimal-build-$CUDA_PKG_VERSION \
  cuda-core-$CUDA_PKG_VERSION \
  cuda-cublas-dev-$CUDA_PKG_VERSION \
  libnccl2=$NCCL_VERSION-1+cuda10.0 \
  libnccl-dev=$NCCL_VERSION-1+cuda10.0 && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends libnvinfer5=5.1.5-1+cuda10.0 && \
 ln -s cuda-10.0 /usr/local/cuda && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

RUN \
 echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf && \
 echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf && \
 echo "/usr/local/cuda/lib64" >> /etc/ld.so.conf.d/cuda.conf && \
 echo "/usr/local/cuda-10.0/lib64" >> /etc/ld.so.conf.d/cuda.conf && \
 echo "/usr/local/cuda/extras/CUPTI/lib64" >> /etc/ld.so.conf.d/cuda.conf && \
 ldconfig

# Make sure that we can run also without GPU.
RUN \
 ln -s /usr/local/cuda/lib64/stubs/libcuda.so /usr/local/cuda/lib64/stubs/libcuda.so.1 && \
 echo "/usr/local/cuda/lib64/stubs" >> /etc/ld.so.conf.d/zzz-cuda-stubs.conf && \
 ldconfig

ENV PATH=/usr/local/nvidia/bin:/usr/local/cuda/bin:${PATH}

ENV NVIDIA_VISIBLE_DEVICES=all
ENV NVIDIA_DRIVER_CAPABILITIES=compute,utility
ENV NVIDIA_REQUIRE_CUDA=cuda>=10.0 brand=tesla,driver>=384,driver<385 brand=tesla,driver>=410,driver<411

ARG org_datadrivendiscovery_public_source_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_SOURCE_COMMIT=$org_datadrivendiscovery_public_source_commit

ARG org_datadrivendiscovery_public_base_digest
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_BASE_DIGEST=$org_datadrivendiscovery_public_base_digest
